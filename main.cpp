
/**
 * #include 是一个指令，用于预处理，会将指定文件的代码插入到当前位置、简单看作java中的import导包即可
 * 下面代码中的cout就是iostream中的方法，要想使用cout，要先把iostream文件导入进来
 */
#include "iostream"

/**
 * 命名空间，类似java中额包名，用来区分不同包下的名称相同的方法名
 * 例如count是std下面的一个方法  要是用cout的话正常的写法是 std::cout
 * 使用了using namespace std 表示可以使用std命名空间下的所有方法  省略掉 sdt::
 */
using namespace std;

int main() {
    /**
     * cout 输出流对象
     * << 是一个插入运算符 表示将字符串插入到cout对象中，或者说插入到cout输出流中，这样就把字符串打印出来了。
     *
     */
    cout << "hello world" << endl;

    /**
     * cin :输入流对象
     */

    int age;//声明一个变量  跟java类似
    cout << "input a number" << endl; //打印个提示信息
    cin >> age;//键盘输入的值赋值给age
    cout << "age is" << age << "\n"; //打印输入的age \n跟endl效果一样 都是换行 endl:end of line1 另起一行

    return 1;
}